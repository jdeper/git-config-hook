#!/bin/sh
if [ ! -d ~/.githook ]
then
  echo "create dir: ~/.githook"
  mkdir -p ~/.githook/
fi
echo '#!/bin/sh' > ~/.githook/post-commit
echo 'exec git rev-list --count HEAD > rev.txt' >> ~/.githook/post-commit
chmod 744 ~/.githook/post-commit
git config --global core.hooksPath "$HOME/.githook"