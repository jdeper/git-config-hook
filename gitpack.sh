#!/bin/sh

REPO=$1
SHA=$2
cd $REPO
REV=`git rev-list --count HEAD`

_PREFIX=`git config --get diffNpatch.local.prefix`
if [ -z "$_PREFIX" ]
then
  _PREFIX="httpdocs"
  echo "set config diffNpatch.local.prefix to $_PREFIX"
  git config diffNpatch.local.prefix $_PREFIX
else
    echo "ziping to $_PREFIX"
fi
PREFIX="../gitpack/$REV/$_PREFIX/"
if [ ! -d $PREFIX ]
then
  echo "create dir: $PREFIX"
  mkdir -p $PREFIX
else
  echo "dir : $PREFIX existed"
fi
ZIPNAME="patch-"$REV".zip"
echo "packing file to "$PREFIX$ZIPNAME
git diff --name-only --diff-filter=d $SHA HEAD | sed  -e "s/^/\"/;s/$/\"/" | xargs zip -r -0 $PREFIX$ZIPNAME
cd $PREFIX
unzip -o $ZIPNAME
rm $ZIPNAME
